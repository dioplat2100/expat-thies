<?php 
require_once '../../utils/db.php';
require_once '../../utils/fonctions.php';

$connexion = getConnexion();
$categories = getAllCategories($connexion);

//var_dump($categories);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter une annonce</title>
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <?php include_once "../../partials/header.php" ?>
    
    <div class="container mt-3">
        <h3>Formulaire de création d'une annonce</h3>
        <form action="" method="post">
            <div class="form-floating mb-3">
                <input
                    type="text"
                    class="form-control"
                    name="title"
                    id="title"
                    placeholder="Ex : Mon iPhone 15 pro max"
                />
                <label for="formId1">Titre de votre annonce</label>
            </div>
            
            <div class="form-floating mb-3">
                <input
                    type="number"
                    class="form-control"
                    name="price"
                    id="price"
                    placeholder=""
                />
                <label for="formId1">Prix de l'annonce</label>
            </div>

            <div class="mb-3">
                <label for="" class="form-label">
                    Description de l'annonce
                </label>
                <textarea class="form-control" name="description" 
                    id="description" rows="3"></textarea>
            </div>
            
            <div class="form-floating mb-3">
                <input
                    type="text"
                    class="form-control"
                    name="image"
                    id="image"
                    placeholder="Ex : Mon iPhone 15 pro max"
                />
                <label for="formId1">Image de votre annonce</label>
            </div>

            <div class="mb-3">
                <label for="" class="form-label">
                    Catégories
                </label>
                <select
                    class="form-select form-select-lg"
                    name="category_id"
                    id="category_id"
                >
                    <?php foreach($categories as $category) : ?>
                        <option value="<?= $category['id'] ?>">
                            <?= $category['name'] ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            
            <div class="d-grid gap-2">
            <button
                type="button"
                class="btn btn-outline-primary"
            >
                Ajouter cette annonce
            </button>
            </div>
            
            
        </form>
    </div>

<script src=".../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>

<?php include_once "../../partials/footer.php"; ?>
</body>
</html>