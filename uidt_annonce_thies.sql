-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 04 juin 2024 à 14:31
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `uidt_annonce_thies`
--

-- --------------------------------------------------------

--
-- Structure de la table `announces`
--

CREATE TABLE `announces` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(1000) NOT NULL DEFAULT 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU',
  `category_id` int(11) DEFAULT NULL,
  `date_publication` datetime DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `announces`
--

INSERT INTO `announces` (`id`, `title`, `price`, `description`, `image`, `category_id`, `date_publication`, `status`) VALUES
(1, 'Annonce n° 1', 1986, 'Ceci est la description de l\'annonce n° 1', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(2, 'Annonce n° 2', 4280, 'Ceci est la description de l\'annonce n° 2', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(3, 'Annonce n° 3', 4713, 'Ceci est la description de l\'annonce n° 3', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(4, 'Annonce n° 4', 5735, 'Ceci est la description de l\'annonce n° 4', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(5, 'Annonce n° 5', 9535, 'Ceci est la description de l\'annonce n° 5', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(6, 'Annonce n° 6', 9516, 'Ceci est la description de l\'annonce n° 6', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(7, 'Annonce n° 7', 6014, 'Ceci est la description de l\'annonce n° 7', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(8, 'Annonce n° 8', 2790, 'Ceci est la description de l\'annonce n° 8', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(9, 'Annonce n° 9', 2835, 'Ceci est la description de l\'annonce n° 9', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(10, 'Annonce n° 10', 4782, 'Ceci est la description de l\'annonce n° 10', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(11, 'Annonce n° 11', 3301, 'Ceci est la description de l\'annonce n° 11', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(12, 'Annonce n° 12', 2214, 'Ceci est la description de l\'annonce n° 12', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(13, 'Annonce n° 13', 7040, 'Ceci est la description de l\'annonce n° 13', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(14, 'Annonce n° 14', 8326, 'Ceci est la description de l\'annonce n° 14', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(15, 'Annonce n° 15', 1389, 'Ceci est la description de l\'annonce n° 15', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(16, 'Annonce n° 16', 3564, 'Ceci est la description de l\'annonce n° 16', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(17, 'Annonce n° 17', 2366, 'Ceci est la description de l\'annonce n° 17', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(18, 'Annonce n° 18', 5227, 'Ceci est la description de l\'annonce n° 18', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(19, 'Annonce n° 19', 4493, 'Ceci est la description de l\'annonce n° 19', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(20, 'Annonce n° 20', 7475, 'Ceci est la description de l\'annonce n° 20', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(21, 'Annonce n° 21', 5639, 'Ceci est la description de l\'annonce n° 21', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(22, 'Annonce n° 22', 7727, 'Ceci est la description de l\'annonce n° 22', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(23, 'Annonce n° 23', 7558, 'Ceci est la description de l\'annonce n° 23', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(24, 'Annonce n° 24', 3695, 'Ceci est la description de l\'annonce n° 24', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(25, 'Annonce n° 25', 2931, 'Ceci est la description de l\'annonce n° 25', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(26, 'Annonce n° 26', 6914, 'Ceci est la description de l\'annonce n° 26', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(27, 'Annonce n° 27', 3426, 'Ceci est la description de l\'annonce n° 27', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(28, 'Annonce n° 28', 5461, 'Ceci est la description de l\'annonce n° 28', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(29, 'Annonce n° 29', 5109, 'Ceci est la description de l\'annonce n° 29', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(30, 'Annonce n° 30', 9017, 'Ceci est la description de l\'annonce n° 30', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(31, 'Annonce n° 31', 2869, 'Ceci est la description de l\'annonce n° 31', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(32, 'Annonce n° 32', 2485, 'Ceci est la description de l\'annonce n° 32', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(33, 'Annonce n° 33', 6149, 'Ceci est la description de l\'annonce n° 33', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(34, 'Annonce n° 34', 2799, 'Ceci est la description de l\'annonce n° 34', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(35, 'Annonce n° 35', 4379, 'Ceci est la description de l\'annonce n° 35', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(36, 'Annonce n° 36', 6185, 'Ceci est la description de l\'annonce n° 36', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(37, 'Annonce n° 37', 9228, 'Ceci est la description de l\'annonce n° 37', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(38, 'Annonce n° 38', 4902, 'Ceci est la description de l\'annonce n° 38', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(39, 'Annonce n° 39', 5430, 'Ceci est la description de l\'annonce n° 39', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(40, 'Annonce n° 40', 8185, 'Ceci est la description de l\'annonce n° 40', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(41, 'Annonce n° 41', 4880, 'Ceci est la description de l\'annonce n° 41', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(42, 'Annonce n° 42', 5550, 'Ceci est la description de l\'annonce n° 42', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(43, 'Annonce n° 43', 8352, 'Ceci est la description de l\'annonce n° 43', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(44, 'Annonce n° 44', 3968, 'Ceci est la description de l\'annonce n° 44', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(45, 'Annonce n° 45', 1232, 'Ceci est la description de l\'annonce n° 45', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(46, 'Annonce n° 46', 2545, 'Ceci est la description de l\'annonce n° 46', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(47, 'Annonce n° 47', 3325, 'Ceci est la description de l\'annonce n° 47', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(48, 'Annonce n° 48', 7061, 'Ceci est la description de l\'annonce n° 48', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(49, 'Annonce n° 49', 1533, 'Ceci est la description de l\'annonce n° 49', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(50, 'Annonce n° 50', 3974, 'Ceci est la description de l\'annonce n° 50', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(51, 'Annonce n° 1', 1229, 'Ceci est la description de l\'annonce n° 1', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(52, 'Annonce n° 2', 5283, 'Ceci est la description de l\'annonce n° 2', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(53, 'Annonce n° 3', 2465, 'Ceci est la description de l\'annonce n° 3', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(54, 'Annonce n° 4', 8925, 'Ceci est la description de l\'annonce n° 4', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(55, 'Annonce n° 5', 5329, 'Ceci est la description de l\'annonce n° 5', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(56, 'Annonce n° 6', 2988, 'Ceci est la description de l\'annonce n° 6', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(57, 'Annonce n° 7', 2926, 'Ceci est la description de l\'annonce n° 7', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(58, 'Annonce n° 8', 6317, 'Ceci est la description de l\'annonce n° 8', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(59, 'Annonce n° 9', 9080, 'Ceci est la description de l\'annonce n° 9', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(60, 'Annonce n° 10', 2507, 'Ceci est la description de l\'annonce n° 10', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(61, 'Annonce n° 11', 4983, 'Ceci est la description de l\'annonce n° 11', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(62, 'Annonce n° 12', 9781, 'Ceci est la description de l\'annonce n° 12', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(63, 'Annonce n° 13', 1512, 'Ceci est la description de l\'annonce n° 13', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(64, 'Annonce n° 14', 3804, 'Ceci est la description de l\'annonce n° 14', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(65, 'Annonce n° 15', 5961, 'Ceci est la description de l\'annonce n° 15', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(66, 'Annonce n° 16', 6724, 'Ceci est la description de l\'annonce n° 16', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(67, 'Annonce n° 17', 5020, 'Ceci est la description de l\'annonce n° 17', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(68, 'Annonce n° 18', 8884, 'Ceci est la description de l\'annonce n° 18', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(69, 'Annonce n° 19', 9889, 'Ceci est la description de l\'annonce n° 19', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(70, 'Annonce n° 20', 9886, 'Ceci est la description de l\'annonce n° 20', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(71, 'Annonce n° 21', 3290, 'Ceci est la description de l\'annonce n° 21', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(72, 'Annonce n° 22', 1214, 'Ceci est la description de l\'annonce n° 22', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(73, 'Annonce n° 23', 5109, 'Ceci est la description de l\'annonce n° 23', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(74, 'Annonce n° 24', 1439, 'Ceci est la description de l\'annonce n° 24', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(75, 'Annonce n° 25', 2497, 'Ceci est la description de l\'annonce n° 25', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(76, 'Annonce n° 26', 3530, 'Ceci est la description de l\'annonce n° 26', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(77, 'Annonce n° 27', 6524, 'Ceci est la description de l\'annonce n° 27', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(78, 'Annonce n° 28', 5330, 'Ceci est la description de l\'annonce n° 28', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(79, 'Annonce n° 29', 4813, 'Ceci est la description de l\'annonce n° 29', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(80, 'Annonce n° 30', 8103, 'Ceci est la description de l\'annonce n° 30', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(81, 'Annonce n° 31', 2672, 'Ceci est la description de l\'annonce n° 31', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(82, 'Annonce n° 32', 9242, 'Ceci est la description de l\'annonce n° 32', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(83, 'Annonce n° 33', 3894, 'Ceci est la description de l\'annonce n° 33', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(84, 'Annonce n° 34', 6986, 'Ceci est la description de l\'annonce n° 34', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(85, 'Annonce n° 35', 5236, 'Ceci est la description de l\'annonce n° 35', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(86, 'Annonce n° 36', 2803, 'Ceci est la description de l\'annonce n° 36', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(87, 'Annonce n° 37', 3648, 'Ceci est la description de l\'annonce n° 37', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(88, 'Annonce n° 38', 8551, 'Ceci est la description de l\'annonce n° 38', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(89, 'Annonce n° 39', 4665, 'Ceci est la description de l\'annonce n° 39', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(90, 'Annonce n° 40', 9610, 'Ceci est la description de l\'annonce n° 40', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(91, 'Annonce n° 41', 4466, 'Ceci est la description de l\'annonce n° 41', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(92, 'Annonce n° 42', 6059, 'Ceci est la description de l\'annonce n° 42', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(93, 'Annonce n° 43', 8726, 'Ceci est la description de l\'annonce n° 43', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(94, 'Annonce n° 44', 7975, 'Ceci est la description de l\'annonce n° 44', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 4, '2024-06-04 11:31:26', 1),
(95, 'Annonce n° 45', 8468, 'Ceci est la description de l\'annonce n° 45', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(96, 'Annonce n° 46', 4610, 'Ceci est la description de l\'annonce n° 46', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(97, 'Annonce n° 47', 4639, 'Ceci est la description de l\'annonce n° 47', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 3, '2024-06-04 11:31:26', 1),
(98, 'Annonce n° 48', 5793, 'Ceci est la description de l\'annonce n° 48', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1),
(99, 'Annonce n° 49', 5129, 'Ceci est la description de l\'annonce n° 49', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 1, '2024-06-04 11:31:26', 1),
(100, 'Annonce n° 50', 5732, 'Ceci est la description de l\'annonce n° 50', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvf9wn1WvKWCp2eCV0atTl56ONzL6TyTPh702UMXqeHag2ZUG0YPch6-XWd2o4S_dK1J4&usqp=CAU', 2, '2024-06-04 11:31:26', 1);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Téléphones'),
(2, 'Voitures'),
(3, 'Ordinateurs'),
(4, 'Locations');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `announces`
--
ALTER TABLE `announces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_category_id` (`category_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `announces`
--
ALTER TABLE `announces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `announces`
--
ALTER TABLE `announces`
  ADD CONSTRAINT `foreign_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
